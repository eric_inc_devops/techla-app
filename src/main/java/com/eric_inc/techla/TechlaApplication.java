package com.eric_inc.techla;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechlaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechlaApplication.class, args);
	}

}
